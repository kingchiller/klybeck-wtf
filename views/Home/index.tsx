import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from './index.module.css'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>The Klybeck - the one drink to many</title>
        <meta
          name="description"
          content="The last drink you order. Guaranteed. Only available at Renée"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        {/* <h1>The Klybeck</h1> */}
        <div className={styles.image}>
          <Image src="/klybeck-recipe.jpg" alt="Klybeck recipe" width={2000} height={1443} />
        </div>
      </main>
    </div>
  )
}

export default Home
